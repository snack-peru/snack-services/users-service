FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 8080
WORKDIR /app
CMD java -jar users-backend*