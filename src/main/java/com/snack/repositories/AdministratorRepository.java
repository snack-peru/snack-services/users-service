package com.snack.repositories;

import com.snack.entities.Administrator;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AdministratorRepository extends JpaRepository<Administrator, Long>{
        //Author: Daniel
        @Query(value = "select * from administrator where id_user = ?1", nativeQuery = true)
        public Administrator getAllAdministratorByIdUser(Long idUser);
    
        //Author: Daniel
        @Transactional
        @Modifying
        @Query(value = "delete from administrator where id_user = ?1 and id_administrator = ?2", nativeQuery = true)
        public void deleteAdministrator(Long idUser, Long idAdministrator);
}