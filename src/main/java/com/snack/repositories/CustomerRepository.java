package com.snack.repositories;

import com.snack.entities.Customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    
    @Query(value = "select id_customer from customer where id_user = ?1", nativeQuery = true)
    Long getCustomerIdByUserId(Long userId);
}