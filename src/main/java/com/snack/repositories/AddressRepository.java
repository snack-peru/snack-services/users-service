package com.snack.repositories;

import java.util.List;

import com.snack.entities.Address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    
    //Author: Sergio
    @Query(value = "select * from address where id_customer = ?1", nativeQuery = true)
    List<Address> getAllAddressByIdCustomer(Long customerId);

    Address findByIsDefault(Boolean isDefault);

    @Query(value = "select * from address where id_customer = ?1 and is_default = ?2", nativeQuery = true)
    Address findDefaultAddressByCustomerId(Long customerId, Boolean flag);
}