package com.snack.dto.request;

import com.snack.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRequest {
    private Long idUser;

    @Size(min = 5, max = 30)
    private String name;

    @Pattern(regexp="\\d{9}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}", message="Invalid phone number!")
    private String phone;

    @Email
    private String email;


    private Date birthDate;

    public UserRequest(User u) {
        this.idUser = u.getIdUser();
        this.name = u.getName();
        this.phone = u.getPhone();
        this.email = u.getEmail();
        this.birthDate = u.getBirthDate();
    }
}