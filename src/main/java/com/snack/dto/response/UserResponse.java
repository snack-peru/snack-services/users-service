package com.snack.dto.response;

import com.snack.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponse {
    private Long idUser;
    private String name;
    private String phone;
    private String email;
    private Date birthDate;

    public UserResponse(User u) {
        this.idUser = u.getIdUser();
        this.name = u.getName();
        this.phone = u.getPhone();
        this.email = u.getEmail();
        this.birthDate = u.getBirthDate();
    }
}