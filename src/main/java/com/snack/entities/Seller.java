package com.snack.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "seller")
public class Seller {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSeller")
    private Long idSeller;

    @Column(name = "trial")
    private Integer trial;

    @Column(name = "trialEnding")
    private String trialEnding;

    @Column(name = "premium")
    private Integer premium;

    @Column(name = "status")
    private Integer status;

    @ManyToOne
    @JoinColumn(name = "idUser")
    private User user;
}