package com.snack.services;

import java.util.List;
import java.util.Optional;

import com.snack.dto.request.AddressRequest;
import com.snack.dto.response.AddressReponse;
import com.snack.entities.Address;
import com.snack.entities.Customer;
import com.snack.repositories.AddressRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    
    @Autowired
    private AddressRepository addressRepository;

    //Author: Sergio
    public List<Address> getAllAddressByIdCustomer(Long customerId) {
        return addressRepository.getAllAddressByIdCustomer(customerId);
    }

    //Author: Sergio
    public AddressReponse findDefaultAddressByCustomerId(Long customerId) {
        Address address = addressRepository.findDefaultAddressByCustomerId(customerId, true);
        return AddressReponse
                .builder()
                .idAddress(address.getIdAddress())
                .description(address.getDescription())
                .isDefault(address.getIsDefault())
                .latitude(address.getLatitude())
                .longitude(address.getLongitude())
                .status(address.getStatus())
                .build();
    }

    //Author: Sergio
    public Address saveCustomerAddress(Long idCustomer, AddressRequest addressRequest) {
        Customer customer = new Customer();
        customer.setIdCustomer(idCustomer);

        Address currentAddressDefault = addressRepository.findByIsDefault(true);
        if (addressRequest.getIsDefault() && currentAddressDefault != null) {
            currentAddressDefault.setIsDefault(false);
            addressRepository.save(currentAddressDefault);
        }

        Address address = Address
                .builder()
                .latitude(addressRequest.getLatitude())
                .longitude(addressRequest.getLongitude())
                .isDefault(addressRequest.getIsDefault())
                .description(addressRequest.getDescription())
                .status(addressRequest.getStatus())
                .customer(customer)
                .build();
        return addressRepository.save(address);
    }

    //Author: Sergio
    public void deleteCustomerAddress(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    // Author: Sergio
    public Long updateDefaultAddress(Long customerId, Long newDefaultAddressId) {
        Long addressIdToChange = null; // se devuelve el id de la dirección que dejará de ser por default

        // Buscar la dirección actual por defecto
        Address address = addressRepository.findDefaultAddressByCustomerId(customerId, true);
        addressIdToChange = address.getIdAddress();

        // Actualizar campo isDefault de la nueva dirección a true
        Optional<Address> optional = addressRepository.findById(newDefaultAddressId);
        if (optional.isPresent()) {
            // Actualizar campos isDefault de la dirección actual a false
            address.setIsDefault(false);
            addressRepository.save(address);

            address = optional.get();
            address.setIsDefault(true);
            addressRepository.save(address);

            return addressIdToChange;
        }
        return null;
    }
}