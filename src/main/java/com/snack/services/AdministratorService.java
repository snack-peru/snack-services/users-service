package com.snack.services;

import com.snack.entities.Administrator;
import com.snack.entities.User;
import com.snack.repositories.AdministratorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministratorService {
    @Autowired
    private AdministratorRepository administratorRepository;

    //Author: Daniel
    public Administrator getAllAdministratorByIdUser(Long idUser) {
        return administratorRepository.getAllAdministratorByIdUser(idUser);
    }

    //Author: Daniel
    public Administrator saveAdministrator(Administrator administrator) {
        User user = new User();
        administrator.setUser(user);
        return administratorRepository.save(administrator);
    }

    //Author: Daniel
    public void deleteAdministrator(Long idUser, Long idAdministrator) {
        administratorRepository.deleteAdministrator(idUser,idAdministrator);
    }
}