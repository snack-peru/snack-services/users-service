package com.snack.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.snack.dto.UserDTO;
import com.snack.dto.request.UserRequest;
import com.snack.entities.User;
import com.snack.repositories.UserRepository;
import com.snack.commons.ErrorMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<UserDTO> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDTO> list = new ArrayList<UserDTO>();

        for (User user : users){
            UserDTO userDTO = new UserDTO();
            userDTO.setIdUser(user.getIdUser());
            userDTO.setName(user.getName());
            userDTO.setEmail(user.getEmail());
            userDTO.setPhone(user.getPhone());
            list.add(userDTO);
        }

        return list;
    }

    public User updateUser(UserRequest user) throws Exception {

        // verify if the user exists
        Optional<User> _userToUpdate = userRepository.findById(user.getIdUser());

        if (!_userToUpdate.isPresent()) {
            throw new Exception(ErrorMessages.userNotFindException);
        }

        User userToUpdate = _userToUpdate.get();

        if (user.getEmail() != null) {
            //update email
            User userEmail = userRepository.findByEmail(user.getEmail());
            if (userEmail != null) {
                throw new Exception(ErrorMessages.emailAlreadyInUse);
            }
            userToUpdate.setEmail(user.getEmail());
        }

        if (user.getBirthDate() != null) {
            //update birthdate
            userToUpdate.setBirthDate(user.getBirthDate());
        }

        if (user.getName() != null) {
            //update name
            userToUpdate.setName(user.getName());
        }

        if (user.getPhone() != null) {
            //update phone
            userToUpdate.setPhone(user.getPhone());
        }

        User updatedUser = userRepository.save(userToUpdate);

        return updatedUser;
    }

}