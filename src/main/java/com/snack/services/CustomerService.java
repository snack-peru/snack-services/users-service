package com.snack.services;

import java.util.Optional;

import com.snack.dto.CustomerDto;
import com.snack.entities.Customer;
import com.snack.entities.User;
import com.snack.repositories.CustomerRepository;
import com.snack.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    public Customer findById(Long customerId) {
        Optional<Customer> optional = customerRepository.findById(customerId);
        if (optional.isPresent()) {
            Customer customer = optional.get();
            return customer;
        }
        return null;
    }

    public CustomerDto loginCustomer(String email, String password) throws Exception {
        //find user by email
        User existingUser = userRepository.findByEmail(email);

        if (existingUser != null) { //if user exists
            //validate password
            if (encoder.matches(password, existingUser.getPassword())) {
                //if password match, create CustomerDto
                Long customerId = customerRepository.getCustomerIdByUserId(existingUser.getIdUser());

                CustomerDto customerDto = new CustomerDto(existingUser.getIdUser(), existingUser.getName(),
                                            existingUser.getPhone(), existingUser.getEmail(), customerId);
                                    
                return customerDto;
            }
            // if it doesn't match, throw exception
            throw new Exception("El usuario o contraseña no coincide");
        }

        //if it doesn't exists, throw exception
        throw new Exception("El correo no existe");
    }

    public CustomerDto registerCustomer(User user) throws Exception {

        // verify if the user exists
        User existingUser = userRepository.findByEmail(user.getEmail());

        if (existingUser == null) {
            // set encrypt password for user
            user.setPassword(encoder.encode(user.getPassword()));

            // save user
            user = userRepository.save(user);

            // save customer
            Customer newCustomer = new Customer(1, user);
            newCustomer = customerRepository.save(newCustomer);
            
            // return customerDto
            return new CustomerDto(user.getIdUser(), user.getName(),user.getPhone(), user.getEmail(), newCustomer.getIdCustomer());
        }

        //if the user exists, it cannot be duplicated
        throw new Exception("El correo ya existe") ;

    }
}