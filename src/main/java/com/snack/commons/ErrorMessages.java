package com.snack.commons;

public final class ErrorMessages {

    private ErrorMessages() {
        // restrict instantiation
    }

    public static final String userNotFindException = "No se encontro el usuario";
    public static final String emailAlreadyInUse = "Email ya esta siendo usado";
}