package com.snack.controllers;

import java.util.List;

import com.snack.dto.request.AddressRequest;
import com.snack.dto.response.AddressReponse;
import com.snack.dto.response.LongResponse;
import com.snack.entities.Address;
import com.snack.services.AddressService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api/address")
public class AddressController {
    
    @Autowired 
    private AddressService addressService;

    //Author: Sergio
    @GetMapping(value = "/{idCustomer}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener direcciónes por un idCustomer",
            notes = "Solo se debe enviar como parámetro el idCustomer",
            responseContainer = "Object")
    public List<Address> getAllAddressByIdCustomer(@PathVariable("idCustomer") Long idCustomer) {
        return addressService.getAllAddressByIdCustomer(idCustomer);
    }

    //Author: Sergio
    @GetMapping(value = "/default/{idCustomer}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener dirección por defecto de un idCustomer",
            notes = "Se envía como parámetro el idCustomer",
            responseContainer = "Object",
            response = Address.class)
    public AddressReponse getDefaultAddressByIdCustomer(@PathVariable("idCustomer") Long idCustomer) {
        return addressService.findDefaultAddressByCustomerId(idCustomer);
    }

    //Author: Sergio
    @PostMapping(value = "/{idCustomer}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Insertar una nueva dirección para un cliente",
            notes = "Se envia como parámetro el idCustomer y la nueva dirección",
            responseContainer = "Object",
            response = Address.class)
    public Address saveCustomerAddress(@PathVariable("idCustomer") Long idCustomer ,@RequestBody AddressRequest address) {
        return addressService.saveCustomerAddress(idCustomer,address);
    }

    //Author: Sergio
    @DeleteMapping(value = "/{addressId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Se eliminar una dirección asociado a un cliente",
            notes = "Se debe enviar como parámetro el id de la dirección",
            responseContainer = "Object",
            response = void.class)
    public void deleteCustomerAddress(@PathVariable("addressId") Long addressId) {
        addressService.deleteCustomerAddress(addressId);
    }

    // Author: Sergio
    @PutMapping(value = "/{customerId}/{addressId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar la dirección por defecto",
            notes = "Se debe enviar como parámetro el id de la dirección que será la nueva por defecto",
            responseContainer = "Object",
            response = LongResponse.class)
    public LongResponse updateDefaultAddress(@PathVariable("customerId") Long customerId,
                                     @PathVariable("addressId") Long newDefaultAddressId) {
        Long idToChange = addressService.updateDefaultAddress(customerId, newDefaultAddressId);

        return LongResponse.builder().id(idToChange).build();
    }
}