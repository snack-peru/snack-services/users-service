package com.snack.controllers;

import com.snack.dto.response.UserResponse;
import com.snack.dto.request.UserRequest;
import com.snack.entities.User;
import com.snack.services.UserService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {
    @Autowired

    private UserService userService;

    @PostMapping(value = "/update")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Actualizar los datos de un usuario",
            notes = "Se envia como parametro el id del usuario y adicionalmente los campos que se desee actualizar",
            responseContainer = "Object",
            response = User.class)
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserRequest user) {
        try {
            User updatedUser = userService.updateUser(user);
            UserResponse response = new UserResponse(updatedUser);
            return new ResponseEntity<UserResponse>(response, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }
}