package com.snack.controllers;

import com.snack.entities.Address;
import com.snack.entities.Administrator;
import com.snack.services.AdministratorService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/administrator")
@CrossOrigin
public class AdministratorController {
    @Autowired 
    private AdministratorService administratorService;
    //Author: Daniel
    @GetMapping(value = "/{idUser}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener usuario administrador",
            notes = "Se envía como parámetro el idUser",
            responseContainer = "Object",
            response = Administrator.class)
    public Administrator getAdministratorByIdUser(@PathVariable("idUser") Long idUser) {
        return administratorService.getAllAdministratorByIdUser(idUser);
    }

    //Author: Daniel
    @PostMapping()
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Guardar un nuevo usuario administrador",
            notes = "",
            responseContainer = "Object",
            response = Administrator.class)
    public Administrator saveAdministrator(@RequestBody Administrator administrator) {
        return administratorService.saveAdministrator(administrator);
    }

    //Author: Daniel
    @DeleteMapping(value = "/{idUser}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Eliminar usuario administrador",
            notes = "Se envía como parámetro el idUser",
            responseContainer = "Object",
            response = void.class)
    public void deleteAdministrator(@PathVariable("idUser") Long idUser, @RequestParam(value = "idAdministrator") Long idAdministrator) {
        administratorService.deleteAdministrator(idUser, idAdministrator);
    }
}