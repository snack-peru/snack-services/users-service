package com.snack.controllers;

import com.snack.dto.CustomerDto;
import com.snack.dto.LoginDto;
import com.snack.entities.Customer;
import com.snack.entities.User;
import com.snack.services.CustomerService;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customer")
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping(value = "/{customerId}")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Obtener usuario customer",
            notes = "Se envía como parámetro el customerId",
            responseContainer = "Object",
            response = Customer.class)
    public Customer getUserById(@PathVariable(name = "customerId") Long customerId) {
        return customerService.findById(customerId);
    }

    @PostMapping(value = "/login")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Email Login Customer",
            notes = "Se envía como requestBody email y password",
            responseContainer = "Object")
    public ResponseEntity<?> loginCustomer(@RequestBody LoginDto loginDto) {
        try {
            CustomerDto customerDto = customerService.loginCustomer(loginDto.getEmail(), loginDto.getPassword());
            return new ResponseEntity<CustomerDto>(customerDto, HttpStatus.OK);
        } catch (Exception e) {
            if (e.getMessage().compareTo("El usuario o contraseña no coincide") == 0) {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage()); 
        }
    }

    @PostMapping(value = "/register")
    @ApiOperation(produces = MediaType.APPLICATION_JSON_VALUE, value = "Registro de nuevo Customer",
            notes = "Se envía como requestBody la clase User",
            responseContainer = "Object")
    public ResponseEntity<?> registerCustomer(@RequestBody User user) {
        try {
            CustomerDto customerDto = customerService.registerCustomer(user);
            return new ResponseEntity<CustomerDto>(customerDto, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }
}